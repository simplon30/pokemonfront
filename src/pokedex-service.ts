import axios from "axios";
import { Pokedex } from "./entities";



export async function fetchPokedex(){
    const response = await axios.get<Pokedex[]>('http://localhost:8000/api/pokedex');
    return response.data;
}