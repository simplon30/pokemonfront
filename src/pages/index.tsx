import Link from "next/link"
import { Button, Card } from "react-bootstrap"


export default function index() {

  function remove() {
    localStorage.removeItem('modal')
  }

  function removeHS() {
    localStorage.removeItem('score')
  }


  return (
    <>
      < div className="container mx-auto mt-5">
        <div className="p-4">

          <div className="d-flex justify-content-between">
            <h1>Config</h1>
            <Link className="arrow" href="/pokemon" style={{ width: '55px', color: "black" }}>
              <svg fill="none" stroke="currentColor" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18"></path>
              </svg>
            </Link>
          </div>

          <div className="d-flex flex-column align-items-center">
            <Card style={{ width: '18rem' }} className="col-6 m-1">
              <Card.Body>
                <Card.Title>Modal</Card.Title>
                <Card.Text>
                  Permet d'afficher de nouveau la modal d'introduction au site
                </Card.Text>
                <Button onClick={remove} variant="primary">Reset</Button>
              </Card.Body>
            </Card>

            <Card style={{ width: '18rem' }} className="col-6 m-1">
              <Card.Body>
                <Card.Title>Score Quiz</Card.Title>
                <Card.Text>
                  Permet de réinitialiser le hight score sur la page Quiz
                </Card.Text>
                <Button onClick={removeHS} variant="primary">Reset</Button>
              </Card.Body>
            </Card>
          </div>
        </div>
      </div>
    </>
  )
}