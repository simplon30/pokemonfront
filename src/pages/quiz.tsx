import { Pokedex } from "@/entities"
import { fetchPokedex } from "@/pokedex-service"
import { GetServerSideProps } from "next"
import { useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useRef } from "react";
import Head from "next/head";

interface Props {
    pokemon: Pokedex[];
}

export default function quiz({ pokemon }: Props) {

    const [userInput, setUserInput] = useState("");
    const [score, setScore] = useState(0);
    const [highScore, setHighScore] = useState(0);
    const [currentPokemon, setCurrentPokemon] = useState(pokemon[0]);

    const [answerOptions, setAnswerOptions] = useState<string[]>([]);
    const [correctAnswer, setCorrectAnswer] = useState<string>("");

    const [form, setForm] = useState(false);
    const [choice, setChoice] = useState(false);

    const inputRef = useRef<HTMLInputElement>(null);

    useEffect(() => {
        inputRef.current?.focus();
    }, [form]);

    useEffect(() => {
        const randomNumber = Math.floor(Math.random() * 251);
        setCurrentPokemon(pokemon[randomNumber]);
        setHighScore(parseInt(localStorage.getItem('score') || "0", 10))
    }, [score, pokemon]);

    useEffect(() => {
        const answerOptions = getRandomAnswerOptions(pokemon, currentPokemon.nom);
        setAnswerOptions(answerOptions);
        setCorrectAnswer(currentPokemon.nom);
    }, [currentPokemon]);    

    const getRandomAnswerOptions = (pokemonList: Pokedex[], correctAnswer: string) => {
        const options = [correctAnswer];
        while (options.length < 4) {
            const randomPokemon = pokemonList[Math.floor(Math.random() * pokemonList.length)];
            if (!options.includes(randomPokemon.nom)) {
                options.push(randomPokemon.nom);
            }
        }
        return options.sort(() => Math.random() - 0.5);
    };

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setUserInput(event.target.value);
    };

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        checkAnswer();
    };

    const checkAnswer = () => {
        let newScore = score;
        if (userInput.toLowerCase() === currentPokemon.nom.toLowerCase()) {
            newScore += 4;
            setUserInput("");
            if (newScore > highScore) {
                localStorage.setItem('score', String(newScore));
                setHighScore(newScore);
            }
        } else {
            const randomNumber = Math.floor(Math.random() * 251);
            setCurrentPokemon(pokemon[randomNumber]);
            setUserInput("");
            newScore = 0;
        }
        setScore(newScore);
        setForm(false)
        setChoice(false)
    };

    const checkMultipleChoiceAnswer = (selectedAnswer: string) => {
        let newScore = score;
        if (selectedAnswer === correctAnswer) {
            newScore += 1;
            if (newScore > highScore) {
                localStorage.setItem('score', String(newScore));
                setHighScore(newScore);
            }
        } else {
            const randomNumber = Math.floor(Math.random() * 251);
            setCurrentPokemon(pokemon[randomNumber]);
            newScore = 0;
        }
        setForm(false)
        setChoice(false)
        setScore(newScore);
    };



    return (
        <>
        <Head>
            <title>Quiz ! Devenez les tous !!</title>
            <meta name="description" content="c'est le content"/>
        </Head>
            <div className="vh-100 vw-100 d-flex flex-column align-items-center justify-content-center">

                <h1>Quel est ce Pokémon ?</h1>
                <img className="img-fluid" src={currentPokemon.sprite} alt="photo de pokemon" style={{ filter: 'contrast(0%) brightness(0)', pointerEvents: "none" }} />

                <div className="d-flex">
                    {!form && !choice &&
                        <Button variant="outline-secondary" className="m-1" onClick={() => setForm(true)}>Répondre par nom</Button>
                    }
                    {!choice && !form &&
                        <Button variant="outline-secondary" className="m-1" onClick={() => setChoice(true)}>Sélectionner une réponse</Button>
                    }
                </div>

                {form &&
                    <Form onSubmit={handleSubmit} className="d-flex flex-column align-items-center pb-2">
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Control
                                ref={inputRef}
                                autoComplete="off"
                                type="text"
                                value={userInput}
                                onChange={handleInputChange}
                                placeholder="Entrez le nom du Pokémon"
                            />
                        </Form.Group>
                        <Button variant="outline-secondary" type="button" onClick={checkAnswer}>
                            Valider
                        </Button>
                    </Form>
                }

                {choice &&
                    <div className="d-flex justify-content-center choice">
                        {answerOptions.map((option, index) => (
                            <Button
                                className="m-1"
                                key={index}
                                variant="outline-secondary"
                                onClick={() => checkMultipleChoiceAnswer(option)}
                            >
                                {option}
                            </Button>
                        ))}
                    </div>
                }

                <p className="pt-4">Votre score : {score}</p>
                <p>Meilleur score : {highScore}</p>
                <div className="d-flex">
                    <p className="m-1">Formulaire : 4 Points</p>
                    <p className="m-1">-</p>
                    <p className="m-1">Choix : 1 Point</p>
                </div>
            </div>
        </>
    );
}


export const getServerSideProps: GetServerSideProps<Props> = async () => {
    return {
        props: {
            pokemon: await fetchPokedex()
        }
    }
}