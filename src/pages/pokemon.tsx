
import { Pokedex } from "@/entities"
import { fetchPokedex } from "@/pokedex-service"
import { GetServerSideProps } from "next"
import Pokeball from "../../public/images/pokeball.svg"
import { useEffect, useState } from "react"
import ModalInfo from "@/components/ModalInfo"
import { Badge, Form } from "react-bootstrap"
import Link from "next/link"
import Head from "next/head"


interface Props {
  pokemon: Pokedex[],
}



export default function Home({ pokemon }: Props) {

  function getTypeClasses(type: string) {
    const typeArray = type.split(','); // Sépare les types par la virgule
    const primaryType = typeArray[0].trim(); // Récupère le premier type et supprime les espaces
    // switch (type) {
    switch (primaryType) {
      case 'Plante':
        return 'bg-plante';
      case 'Feu':
        return 'bg-feu';
      case 'Eau':
        return 'bg-eau';
      case 'Normal':
        return 'bg-normal';
      case 'Vol':
        return 'bg-normal';
      case 'Électrik':
        return 'bg-électrik';
      case 'Insecte':
        return 'bg-insecte';
      case 'Roche':
        return 'bg-roche';
      case 'Poison':
        return 'bg-poison';
      case 'Fée':
        return 'bg-fée';
      case 'Sol':
        return 'bg-sol';
      case 'Spectre':
        return 'bg-spectre';
      case 'Psy':
        return 'bg-psy';
      case 'Acier':
        return 'bg-acier';
      case 'Combat':
        return 'bg-combat';
      case 'Glace':
        return 'bg-glace';
      case 'Ténèbres':
        return 'bg-tenebres';
      case 'Dragon':
        return 'bg-dragon';
      case 'Insecte,Vol':
        return 'bg-insecte-vol';
      case 'Normal,Vol':
        return 'bg-normal-vol';
      case 'Insecte,Poison':
        return 'bg-insect-poison';
      case 'Roche,Sol':
        return 'bg-roche-sol';
      case 'Poison,Vol':
        return 'bg-poison-vol';
      case 'Normal,Fée':
        return 'bg-normal-fée';
      case 'Fée,Vol':
        return 'bg-fée-vol';
      case 'Eau,Sol':
        return 'bg-eau-sol';
      case 'Spectre,Poison':
        return 'bg-spectre-poison';
      case 'Acier,Sol':
        return 'bg-acier-sol';
      case 'Plante,Poison':
        return 'bg-plante-poison';
      case 'Plante,Vol':
        return 'bg-plante-vol';
      case 'Insecte,Plante':
        return 'bg-insecte-plante';
      case 'Eau,Combat':
        return 'bg-eau-combat';
      case 'Eau,Vol':
        return 'bg-eau-vol';
      case 'Eau,Psy':
        return 'bg-eau-psy';
      case 'Insecte,Acier':
        return 'bg-insecte-acier';
      case 'Poison,Sol':
        return 'bg-poison-sol';
      case 'Plante,Psy':
        return 'bg-plante-psy';
      case 'Insecte,Combat':
        return 'bg-insecte-combat';
      case 'Électrik,Acier':
        return 'bg-electrik-acier';
      case 'Eau,Fée':
        return 'bg-eau-fée';
      case 'Normal,Psy':
        return 'bg-normal-psy';
      case 'Glace,Psy':
        return 'bg-glace-psy';
      case 'Psy,Fée':
        return 'bg-psy-fée';
      case 'Psy,Vol':
        return 'bg-psy-vol';
      case 'Eau,Poison':
        return 'bg-eau-poison';
      case 'Insecte,Roche':
        return 'bg-insecte-roche';
      case 'Eau,Glace':
        return 'bg-eau-glace';
      case 'Eau,Électrik':
        return 'bg-eau-electrik';
      case 'Eau,Roche':
        return 'bg-eau-roche';
      case 'Eau,Dragon':
        return 'bg-eau-dragon';
      case 'Sol,Vol':
        return 'bg-sol-vol';
      case 'Glace,Vol':
        return 'bg-glace-vol';
      case 'Glace,Sol':
        return 'bg-glace-sol';
      case 'Acier,Vol':
        return 'bg-acier-vol';
      case 'Sol,Roche':
        return 'bg-sol-roche';
      case 'Ténèbres,Vol':
        return 'bg-tenebres-vol';
      case 'Ténèbres,Feu':
        return 'bg-tenebres-feu';
      case 'Ténèbres,Glace':
        return 'bg-tenebres-glace';
      case 'Feu,Roche':
        return 'bg-feu-roche';
      case 'Roche,Eau':
        return 'bg-roche-eau';
      case 'Roche,Vol':
        return 'bg-roche-vol';
      case 'Feu,Vol':
        return 'bg-feu-vol';
      case 'Électrik,Vol':
        return 'bg-electrik-vol';
      case 'Dragon,Vol':
        return 'bg-dragon-vol';
      case 'Roche,Ténèbres':
        return 'bg-roche-tenebre';
      case 'Psy,Plante':
        return 'bg-psy-plante';
      default:
        return 'bg-black';
    }
  }

  const types: string[] = [
    'Normal', 'Feu', 'Eau', 'Électrik', 'Plante', 'Glace', 'Combat',
    'Poison', 'Sol', 'Vol', 'Psy', 'Insecte', 'Roche', 'Spectre',
    'Dragon', 'Ténèbres', 'Acier', 'Fée'
  ];

  const [modalShow, setModalShow] = useState(false);

  const [searchTerm, setSearchTerm] = useState('');
  const [selectedType, setSelectedType] = useState<string[]>([]);

  const filteredByName = pokemon.filter(pokemon =>
    pokemon.nom.toLowerCase().includes(searchTerm.toLowerCase())
  );

  const filteredByType = selectedType
    ? filteredByName.filter(pokemon => pokemon.type.includes(String(selectedType)))
    : filteredByName;

  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const toggleMenu = () => {
    setIsMenuOpen(!isMenuOpen);
  };

  const handleTypeClick = (type: string) => {
    if (selectedType.includes(type)) {
      setSelectedType(selectedType.filter(t => t !== type));
      toggleMenu()
    } else {
      setSelectedType([type]);
      toggleMenu()
    }
  };



  useEffect(() => {
    if (!localStorage.getItem('modal')) {
      setModalShow(true)
      localStorage.setItem('modal', "true");
    }
  }, [])



  return (
    <>
      <Head>
        <title>Pokedex </title>
        <meta name="description" content="c'est le content" />
      </Head>
      <ModalInfo
        show={modalShow}
        onHide={() => setModalShow(false)}
        className="modal-lg d-flex justify-content-center"
      />

      < div className="container mx-auto mt-5">
        <div className="p-4">

          <div className="d-flex justify-content-between align-items-center">
            <h1>Pokedex de Johto</h1>
            <div className="d-flex align-items-center" >

              <Link className="icon" href="/quiz" style={{ width: '55px', color: "black" }}>
                <svg fill="none" stroke="currentColor" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                  <path stroke-linecap="round" stroke-linejoin="round" d="M14.25 6.087c0-.355.186-.676.401-.959.221-.29.349-.634.349-1.003 0-1.036-1.007-1.875-2.25-1.875s-2.25.84-2.25 1.875c0 .369.128.713.349 1.003.215.283.401.604.401.959v0a.64.64 0 01-.657.643 48.39 48.39 0 01-4.163-.3c.186 1.613.293 3.25.315 4.907a.656.656 0 01-.658.663v0c-.355 0-.676-.186-.959-.401a1.647 1.647 0 00-1.003-.349c-1.036 0-1.875 1.007-1.875 2.25s.84 2.25 1.875 2.25c.369 0 .713-.128 1.003-.349.283-.215.604-.401.959-.401v0c.31 0 .555.26.532.57a48.039 48.039 0 01-.642 5.056c1.518.19 3.058.309 4.616.354a.64.64 0 00.657-.643v0c0-.355-.186-.676-.401-.959a1.647 1.647 0 01-.349-1.003c0-1.035 1.008-1.875 2.25-1.875 1.243 0 2.25.84 2.25 1.875 0 .369-.128.713-.349 1.003-.215.283-.4.604-.4.959v0c0 .333.277.599.61.58a48.1 48.1 0 005.427-.63 48.05 48.05 0 00.582-4.717.532.532 0 00-.533-.57v0c-.355 0-.676.186-.959.401-.29.221-.634.349-1.003.349-1.035 0-1.875-1.007-1.875-2.25s.84-2.25 1.875-2.25c.37 0 .713.128 1.003.349.283.215.604.401.96.401v0a.656.656 0 00.658-.663 48.422 48.422 0 00-.37-5.36c-1.886.342-3.81.574-5.766.689a.578.578 0 01-.61-.58v0z"></path>
                </svg>
              </Link>

              <Link className="icon" href="/" style={{ width: '55px', color: "black" }}>
                <svg fill="none" stroke="currentColor" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                  <path stroke-linecap="round" stroke-linejoin="round" d="M9.594 3.94c.09-.542.56-.94 1.11-.94h2.593c.55 0 1.02.398 1.11.94l.213 1.281c.063.374.313.686.645.87.074.04.147.083.22.127.324.196.72.257 1.075.124l1.217-.456a1.125 1.125 0 011.37.49l1.296 2.247a1.125 1.125 0 01-.26 1.431l-1.003.827c-.293.24-.438.613-.431.992a6.759 6.759 0 010 .255c-.007.378.138.75.43.99l1.005.828c.424.35.534.954.26 1.43l-1.298 2.247a1.125 1.125 0 01-1.369.491l-1.217-.456c-.355-.133-.75-.072-1.076.124a6.57 6.57 0 01-.22.128c-.331.183-.581.495-.644.869l-.213 1.28c-.09.543-.56.941-1.11.941h-2.594c-.55 0-1.02-.398-1.11-.94l-.213-1.281c-.062-.374-.312-.686-.644-.87a6.52 6.52 0 01-.22-.127c-.325-.196-.72-.257-1.076-.124l-1.217.456a1.125 1.125 0 01-1.369-.49l-1.297-2.247a1.125 1.125 0 01.26-1.431l1.004-.827c.292-.24.437-.613.43-.992a6.932 6.932 0 010-.255c.007-.378-.138-.75-.43-.99l-1.004-.828a1.125 1.125 0 01-.26-1.43l1.297-2.247a1.125 1.125 0 011.37-.491l1.216.456c.356.133.751.072 1.076-.124.072-.044.146-.087.22-.128.332-.183.582-.495.644-.869l.214-1.281z"></path>
                  <path stroke-linecap="round" stroke-linejoin="round" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path>
                </svg>
              </Link>

            </div>
          </div>

          <Form className="menu">
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Control value={searchTerm} onChange={e => setSearchTerm(e.target.value)} type="text" placeholder="Cherche un Pokemon !" />
            </Form.Group>
          </Form>

          <div className="menu">

            {/* Menu Mobile */}
            <div className="d-lg-none d-flex justify-content-center flex-column">
              <button
                className="navbar-toggler"
                type="button"
                onClick={toggleMenu}
                style={{ width: "100%" }}
              >
                <Badge
                  bg={'secondary-subtle'}
                  text={'dark'}
                  onClick={toggleMenu}
                  className="p-3 me-2 mb-2"
                  style={{ cursor: 'pointer', width: "90%" }}
                >
                  Filtrer par types
                </Badge>
              </button>

              {isMenuOpen && (
                <div className="menu">
                  <ul className="list-group">
                    {types.map(type => (
                      <li
                        key={type}
                        className={`list-group-item ${selectedType.includes(type) ? `${getTypeClasses(type)} text-white` : ''}`}
                        onClick={() => handleTypeClick(type)}
                      >
                        {type}
                      </li>
                    ))}
                  </ul>
                </div>
              )}

            </div>

            {/* Menu Desktop */}
            <div className="d-none d-lg-block">

              <Badge
                bg={selectedType && selectedType.length === 0 ? 'secondary-subtle' : 'light'}
                text={selectedType && selectedType.length === 0 ? 'dark' : 'dark'}
                onClick={() => setSelectedType([])}
                className="p-3 me-2 mb-2"
                style={{ cursor: 'pointer' }}
              >
                Tous les types
              </Badge>

              {types.map(type => (
                <Badge
                  key={type}
                  bg={selectedType && selectedType.includes(type) ? '' : 'light'}
                  text={selectedType && selectedType.includes(type) ? 'white' : 'dark'}
                  onClick={() => setSelectedType([type])}
                  className={`col p-3 me-2 mb-2 ${getTypeClasses(type)}`}
                  style={{ cursor: 'pointer' }}
                >
                  {type}
                </Badge>
              ))}
            </div>

          </div>

          <div className="d-flex flex-wrap justify-content-center gap-4">

            {filteredByType.map(item => {

              const isShiny = Math.floor(Math.random() * 252) === 1;
              const formattedId = `#${String(item.id).padStart(3, '0')}`;

              return (
                <>
                  <a href={`/pokemon/${item.nom}`} style={{ border: `solid 2px` }} className={`${getTypeClasses(item.type)} pokemonCard d-flex flex-column align-items-center col-md-2 col-sm-3 col-5 rounded-5 position-relative text-white text-decoration-none shadow-lg `}>

                    <p className="mt-2 text-dark">{formattedId}</p>
                    <p className="fw-bold fs-5">{item.nom}</p>

                    <div className="position-relative">
                      {isShiny ? (
                        <img className="img-fluid position-relative z-1 zoom" src={item.spriteShiny} alt={`Photo shiny de ${item.nom}`} />
                      ) : (
                        <img className="img-fluid position-relative z-1 zoom" src={item.sprite} alt={`Photo de ${item.nom}`} />
                      )}
                      <div className="position-absolute top-0 start-0 z-0 m-2">
                        <Pokeball style={{ width: "100%", zIndex: 0 }} alt="Pokeball icon" className="img-fluid pokeballSvg" />
                      </div>
                    </div>

                    <div>
                      <ul className="mt-4 p-0 d-flex flex-column">
                        {item.type.split(',').map((type, index) => (
                          <li key={index} className="px-4 py-1 small fw-semibold" style={{ backgroundColor: 'rgba(0, 0, 0, 0.3)', borderRadius: '1rem', display: 'inline-block', marginBottom: '5px' }}>
                            {type}
                          </li>
                        ))}
                      </ul>
                    </div>
                  </a>
                </>
              )
            }
            )}

          </div>
        </div>
      </div >
    </>
  )
}

export const getServerSideProps: GetServerSideProps<Props> = async () => {
  return {
    props: {
      pokemon: await fetchPokedex()
    }
  }
}