
import HeightWeight from "@/components/HeightWeight";
import StatsBar from "@/components/StatsBar";
import { Pokemon } from "@/entities";
import { fetchPokedex } from "@/pokedex-service";
import { fetchPokemonById, fetchPokemonByName } from "@/pokemon-service"
import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router"
import { useEffect, useState } from "react";
import { Nav, Tab } from "react-bootstrap";



interface Props {
    pokemon: Pokemon
}

// Fonction getStaticPaths pour générer les chemins des pages des Pokémon
export async function getStaticPaths() {
    // Récupérer les noms de tous les Pokémon depuis votre source de données
    const allPokemon = await fetchPokedex();

    // Générer les chemins des pages pour chaque Pokémon
    const paths = allPokemon.map((pokemon) => ({
        params: { id: pokemon.nom }, // slug doit correspondre au paramètre dans la page [slug].js
    }));

    // Retourner les chemins générés
    return {
        paths,
        fallback: false, // true si vous voulez gérer des cas où le Pokémon n'existe pas
    };
}

// Fonction getStaticProps pour récupérer les données du Pokémon spécifié
export async function getStaticProps({ params }: any) {
    // Récupérer les détails du Pokémon en utilisant son nom (slug)
    const pokemon = await fetchPokemonByName(params.id);

    // Retourner les données du Pokémon comme props
    return {
        props: {
            pokemon,
        },
    };
}


export default function id({ pokemon }: Props) {

    const router = useRouter();
    const { id } = router.query;


    const formattedId = `#${String(pokemon.pokemon.id).padStart(3, '0')}`;
    const formattedType = pokemon.pokemon.type.split('|');


    function getTypeClasses(type: string) {
        const typeArray = type.split('|'); // Sépare les types par la virgule
        const primaryType = typeArray[0].trim(); // Récupère le premier type et supprime les espaces
        switch (primaryType) {
            case 'Plante':
                return 'bg-plante';
            case 'Feu':
                return 'bg-feu';
            case 'Eau':
                return 'bg-eau';
            case 'Normal':
                return 'bg-normal';
            case 'Électrik':
                return 'bg-électrik';
            case 'Insecte':
                return 'bg-insecte';
            case 'Roche':
                return 'bg-roche';
            case 'Poison':
                return 'bg-poison';
            case 'Fée':
                return 'bg-fée';
            case 'Sol':
                return 'bg-sol';
            case 'Spectre':
                return 'bg-spectre';
            case 'Psy':
                return 'bg-psy';
            case 'Acier':
                return 'bg-acier';
            case 'Combat':
                return 'bg-combat';
            case 'Glace':
                return 'bg-glace';
            case 'Ténèbres':
                return 'bg-tenebres';
            case 'Dragon':
                return 'bg-dragon';
            default:
                return 'bg-black';
        }
    }

    const [nextPkmn, setNextPkmn] = useState<Pokemon>();
    const [previousPkmn, setPreviousPkmn] = useState<Pokemon>();



    useEffect(() => {
        if (pokemon.pokemon.id) {
            fetchPokemonById(pokemon.pokemon.id + 1)
                .then(data => setNextPkmn(data))
            fetchPokemonById(pokemon.pokemon.id - 1)
                .then(data => setPreviousPkmn(data))
        }
        if (pokemon.evolution && !pokemon.evolution[0] && activeTab === 'onglet4') {
            setActiveTab('onglet1')
        }
    }, [id])

    const [activeTab, setActiveTab] = useState('onglet1');

    function handleTabSelect(tabKey: any) {
        setActiveTab(tabKey);
    };

    function genderRate(genderRate: number) {
        switch (genderRate) {
            case -1:
                return "Asexué";
            case 0:
                return "100% mâle";
            case 1:
                return "12.5% femelle ; 87.5% mâle";
            case 4:
                return "50% femelle ; 50% mâle";
            case 8:
                return "100% femelle";
            case 6:
                return "75 % femelle ; 25 % mâle";
            case 2:
                return "25 % femelle ; 75 % mâle";
            default:
                return "Taux de genre non reconnu";
        }
    }




    return (
        <>
            <Head>
                <title>{`Pokemon : ${pokemon.pokemon.nom}`}</title>
                <meta name="description" content="c'est le content" />
            </Head>
            <div className={`${getTypeClasses(pokemon.pokemon.type)}`}>
                <div className=" container p-2 pb-0">

                    <Link href="/pokemon">
                        <svg className="arrow" fill="none" width="64" stroke="white" strokeWidth="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                            <path strokeLinecap="round" strokeLinejoin="round" d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18"></path>
                        </svg>
                    </Link>

                    {/* Nom + ID */}
                    <div className="d-flex justify-content-between text-white fw-bold fs-4 align-items-center header">
                        <p style={{ marginBottom: "0" }} className="fs-1">{pokemon.pokemon.nom}</p>
                        <p style={{ marginBottom: "0" }}>{formattedId}</p>
                    </div>

                    {/* Type + Genera + Shape */}
                    <div className="d-flex justify-content-between">
                        <ul className="mt-4 p-0 d-flex flex-column align-items-start">
                            {formattedType.map((type, index) => (
                                <li key={index} className="px-4 py-1 small fw-semibold text-white" style={{ backgroundColor: 'rgba(0, 0, 0, 0.3)', borderRadius: '1rem', display: 'inline-block', marginBottom: '5px' }}>
                                    {type}
                                </li>
                            ))}
                        </ul>

                        <div className="d-flex justify-content-between align-items-center text-white">
                            <div className="mt-2 fw-bold text-right opacity-75 fs-6">
                                <p>{pokemon.pokemon.genera}</p>
                                <p>{pokemon.pokemon.shape}</p>
                            </div>
                        </div>
                    </div>

                    {/* Image */}
                    {/* transition */}
                    <div className="d-flex justify-content-center align-items-center z-1 position-relative bodyImg" style={{ marginBottom: "-5.5em" }}>
                        <div className="col-2 d-flex justify-content-center align-items-center">
                            {previousPkmn &&
                                <Link href={`/pokemon/${previousPkmn.pokemon.nom}`}>
                                    <img className="nextPkmn img-fluid" src={previousPkmn.pokemon.sprite} alt={`photo de  ${previousPkmn.pokemon.nom}`} style={{ filter: 'contrast(0%) brightness(0.7)' }} />
                                </Link>
                            }
                        </div>

                        <div className="col-6 d-flex justify-content-center align-items-center">
                            <img className="img-fluid" src={pokemon.pokemon.sprite} alt={`photo de  ${pokemon.pokemon.nom}`} />
                        </div>

                        <div className="col-2 d-flex justify-content-center align-items-center">
                            {nextPkmn &&
                                <Link href={`/pokemon/${nextPkmn.pokemon.nom}`}>
                                    <img className="nextPkmn img-fluid" src={nextPkmn.pokemon.sprite} alt={`photo de  ${nextPkmn.pokemon.nom}`} style={{ filter: 'contrast(0%) brightness(0.7)' }} />
                                </Link>
                            }
                        </div>
                    </div>

                    <div className="d-flex justify-content-center">
                        <div className="bodyInfo" style={{ backgroundColor: "white", width: "50vw", borderTopLeftRadius: "15px", borderTopRightRadius: "15px" }}>

                            <Tab.Container activeKey={activeTab} onSelect={handleTabSelect} >
                                <Nav variant="tabs" className="z-2 position-relative mb-5 pt-3 d-flex justify-content-around">
                                    <Nav.Item>
                                        <Nav.Link eventKey="onglet1">
                                            <svg className="svgId" aria-hidden="true" fill="none" stroke="currentColor" strokeWidth="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M11.25 11.25l.041-.02a.75.75 0 011.063.852l-.708 2.836a.75.75 0 001.063.853l.041-.021M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-9-3.75h.008v.008H12V8.25z" strokeLinecap="round" strokeLinejoin="round"></path>
                                            </svg>
                                            A propos
                                        </Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link eventKey="onglet2">
                                            <svg className="svgId" aria-hidden="true" fill="none" stroke="currentColor" strokeWidth="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M7.5 14.25v2.25m3-4.5v4.5m3-6.75v6.75m3-9v9M6 20.25h12A2.25 2.25 0 0020.25 18V6A2.25 2.25 0 0018 3.75H6A2.25 2.25 0 003.75 6v12A2.25 2.25 0 006 20.25z" strokeLinecap="round" strokeLinejoin="round"></path>
                                            </svg>
                                            Stats
                                        </Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link eventKey="onglet3">
                                            <svg className="svgId" aria-hidden="true" fill="none" stroke="currentColor" strokeWidth="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M3.75 13.5l10.5-11.25L12 10.5h8.25L9.75 21.75 12 13.5H3.75z" strokeLinecap="round" strokeLinejoin="round"></path>
                                            </svg>
                                            Formes
                                        </Nav.Link>
                                    </Nav.Item>
                                    {pokemon.evolution && pokemon.evolution[0] &&
                                        <Nav.Item>
                                            <Nav.Link eventKey="onglet4">
                                                <svg className="svgId" aria-hidden="true" fill="none" stroke="currentColor" strokeWidth="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M4.5 12.75l7.5-7.5 7.5 7.5m-15 6l7.5-7.5 7.5 7.5" strokeLinecap="round" strokeLinejoin="round"></path>
                                                </svg>
                                                Evolution
                                            </Nav.Link>
                                        </Nav.Item>
                                    }
                                </Nav>

                                <Tab.Content>
                                    {/* A Propos */}
                                    <Tab.Pane eventKey="onglet1">

                                        <p>{pokemon.pokemon.description}</p>

                                        <HeightWeight Height={pokemon.pokemon.height} Weight={pokemon.pokemon.weight} />

                                        <div className="">
                                            <div className="fw-bold text-right opacity-50 fs-6">
                                                <p>Talents :</p>
                                            </div>
                                            <ul className="list-group">
                                                {pokemon.pokemon && pokemon.pokemon.abilities.split('|').map((ability, index) => (
                                                    <li className="list-group-item" key={index}>
                                                        {ability} : {pokemon.pokemon.abilitiesDesc.split('|')[index]}
                                                    </li>
                                                ))}
                                            </ul>
                                        </div>


                                        <div className="mt-3 d-flex">
                                            <div className="fw-bold text-right opacity-50 fs-6">
                                                <p>Taux de Capture :</p>
                                            </div>
                                            <p className="ms-1">
                                                {pokemon.pokemon.captureRate}%
                                            </p>
                                        </div>

                                        <div className="mt-3 d-flex">
                                            <div className="fw-bold text-right opacity-50 fs-6">
                                                <p>Sexe :</p>
                                            </div>
                                            <p className="ms-1">
                                                {genderRate(pokemon.pokemon.genderRate)}
                                            </p>
                                        </div>

                                        <div className="mt-3">
                                            <div className="fw-bold text-right opacity-50 fs-6">
                                                <p>Localisation :</p>
                                            </div>
                                            <ul className="list-group">
                                                {pokemon.pokemon && pokemon.pokemon.encounters.split('|').map((encounter, index) => (
                                                    <li className="list-group-item col-6" key={index}>
                                                        {encounter}
                                                    </li>
                                                ))}
                                            </ul>
                                        </div>

                                    </Tab.Pane>

                                    {/* Stats */}
                                    <Tab.Pane eventKey="onglet2">

                                        <StatsBar statsTxt="PV" pkmnStats={pokemon.pokemon.Hp} />
                                        <StatsBar statsTxt="Attaque" pkmnStats={pokemon.pokemon.Attack} />
                                        <StatsBar statsTxt="Défense" pkmnStats={pokemon.pokemon.Defense} />
                                        <StatsBar statsTxt="Atq. Spé." pkmnStats={pokemon.pokemon.specialAttack} />
                                        <StatsBar statsTxt="Déf. Spé." pkmnStats={pokemon.pokemon.specialDefense} />
                                        <StatsBar statsTxt="Vitesse" pkmnStats={pokemon.pokemon.speed} />

                                    </Tab.Pane>

                                    {/* Formes */}
                                    <Tab.Pane eventKey='onglet3'>
                                        <div className="FormesId d-flex">

                                            <div className="col-6 d-flex align-items-center justify-content-center flex-wrap">
                                                <img className="img-fluid" src={pokemon.pokemon.sprite} alt="" />
                                                <p className="mt-2 fw-bold text-right fs-6">Normal</p>
                                            </div>
                                            <div className="col-6 d-flex align-items-center justify-content-center flex-wrap">
                                                <img className="img-fluid" src={pokemon.pokemon.spriteShiny} alt="" />
                                                <p className="mt-2 fw-bold text-right fs-6">Chromatique</p>
                                            </div>

                                        </div>
                                    </Tab.Pane>

                                    {/* Evolution? */}
                                    <Tab.Pane eventKey="onglet4">

                                        {pokemon.evolution && pokemon.evolution.map(pkmnEvo =>

                                            <div key={pkmnEvo.id} className="d-flex align-items-center">


                                                <div className="col d-flex align-items-center flex-column">
                                                    <img className="img-fluid" src={pkmnEvo.evolveFromImg} alt={`photo de ${pkmnEvo.evolveFromName}`} />
                                                    <p>{pkmnEvo.evolveFromName}</p>
                                                </div>

                                                <div className="col d-flex align-items-center flex-column imgEvo">
                                                    <svg fill="none" width="64" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                                                        <path strokeLinecap="round" strokeLinejoin="round" d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3"></path>
                                                    </svg>

                                                    <div className="col d-flex align-items-center flex-column">
                                                        {pkmnEvo.evolveToCondition && pkmnEvo.evolveToLvl &&
                                                            <p>Lvl {pkmnEvo.evolveToLvl}</p>}

                                                        {pkmnEvo.evolveToBonheur &&
                                                            <p>Bonheur</p>}

                                                        {pkmnEvo.evolveToBonheur && pkmnEvo.evolveToTimeOfDay &&
                                                            <p>{pkmnEvo.evolveToTimeOfDay.includes("day") ? "Le jour" : pkmnEvo.evolveToTimeOfDay === "night" ? "La nuit" : null}</p>}

                                                        {pkmnEvo.evolveToCondition && pkmnEvo.evolveToCondition.includes("Roche Royale|trade")
                                                            ? "Échange + Roche Royale"
                                                            : pkmnEvo.evolveToCondition && pkmnEvo.evolveToCondition.includes("Écaille Draco|trade")
                                                                ? "Échange + Écaille Draco"
                                                                : pkmnEvo.evolveToCondition && pkmnEvo.evolveToCondition.includes("Améliorator|trade")
                                                                    ? "Échange + Améliorator"
                                                                    : pkmnEvo.evolveToCondition === "trade"
                                                                        ? "Échange"
                                                                        : pkmnEvo.evolveToCondition === "level-up"
                                                                            ? null
                                                                            : pkmnEvo.evolveToCondition && pkmnEvo.evolveToCondition.includes("level-up|")
                                                                                ? pkmnEvo.evolveToCondition && pkmnEvo.evolveToCondition.split("level-up|")[1]
                                                                                : pkmnEvo.evolveToCondition}
                                                    </div>
                                                </div>

                                                <div className="col d-flex align-items-center flex-column">
                                                    <img className="img-fluid" src={pkmnEvo.evolveToImage} alt={`photo de ${pkmnEvo.evolveToName}`} />
                                                    <p>{pkmnEvo.evolveToName}</p>
                                                </div>


                                            </div>
                                        )}

                                    </Tab.Pane>
                                </Tab.Content>
                            </Tab.Container>
                        </div>
                    </div>
                </div>
            </div >
        </>

    )
}

// export const getServerSideProps: GetServerSideProps<Props> = async (context) => {
//     const nom = context.query.id
//     return {
//         props: {
//             pokemon: await fetchPokemonByName(String(nom))
//         }
//     }
// }