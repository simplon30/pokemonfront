import { JSX, DetailedHTMLProps, HTMLAttributes, RefObject, ReactNode } from 'react';
import { Button } from 'react-bootstrap';
import Modal, { ModalProps } from 'react-bootstrap/Modal';
import { Omit, BsPrefixProps } from 'react-bootstrap/esm/helpers';



export default function ModalInfo(props: JSX.IntrinsicAttributes & Omit<Omit<DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>, "ref"> & { ref?: ((instance: HTMLDivElement | null) => void) | RefObject<HTMLDivElement> | null | undefined; }, BsPrefixProps<"div"> & ModalProps> & BsPrefixProps<"div"> & ModalProps & { children?: ReactNode; }) {
  return (
    <div>
      <Modal
        {...props}
        aria-labelledby="contained-modal-title-vcenter"
        centered
        animation={false}
        backdrop="static"
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter" className='text-white'>
            Bienvenue dans le fabuleux monde des Pokémon !
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className='d-flex align-items-center'>
          <div className='d-flex align-items-center'>
            <img className="img-fluid imgModal" style={{ height: "150px" }} src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/250.png" alt={`Photo de Ho-Oh`} />
            <div>
              <p>
                Permettez-moi de vous présenter le Pokédex de Johto, votre fidèle compagnon dans cette exploration extraordinaire.
              </p>
              <p>
                C'est une fenêtre vers la diversité étonnante de la région de Johto, avec ses plaines verdoyantes, ses montagnes escarpées et ses rivières cristallines.
                Chaque page de ce Pokédex renferme des connaissances essentielles sur les Pokémon que vous rencontrerez lors de votre périple.
              </p>
              <p>
                Attrapez-les tous, jeunes dresseurs, et que votre voyage soit couronné de succès et de découvertes captivantes !
              </p>
            </div>
            <img className="img-fluid imgModal" style={{ height: "150px" }} src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/249.png" alt={`Photo de Lugia`} />
          </div>
        </Modal.Body>
        <Modal.Footer className='d-flex justify-content-center'>
          <Button variant="light" onClick={props.onHide}>C'est parti !</Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}