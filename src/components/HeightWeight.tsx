

interface Props {
    Height: string;
    Weight: string;
}

export default function HeightWeight({ Height, Weight }: Props) {


    return (
        <>
            <div className="HeightWeight d-flex justify-content-around align-items-center" style={{ borderRadius: "15px", height: "6em", marginBottom : '1rem' , boxShadow :" 0px 0px 15px rgba(0, 0, 0, 0.5)" }}>

                <div className="d-flex align-items-center flex-column">
                    <div className="d-flex align-items-center mb-1">
                        <svg style={{ width: "2em" }} aria-hidden="true" fill="none" stroke="currentColor" strokeWidth="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M3 16.5v2.25A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75V16.5m-13.5-9L12 3m0 0l4.5 4.5M12 3v13.5" strokeLinecap="round" strokeLinejoin="round"></path>
                        </svg>
                        <p className="m-auto ms-2">Taille</p>
                    </div>
                    <p className="mb-0 mt-1">{Number(Height) / 10} mètres</p>
                </div>

                <div className="d-flex align-items-center flex-column">
                    <div className="d-flex align-items-center mb-1">
                        <svg style={{ width: "2em" }} aria-hidden="true" fill="none" stroke="currentColor" strokeWidth="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 3v17.25m0 0c-1.472 0-2.882.265-4.185.75M12 20.25c1.472 0 2.882.265 4.185.75M18.75 4.97A48.416 48.416 0 0012 4.5c-2.291 0-4.545.16-6.75.47m13.5 0c1.01.143 2.01.317 3 .52m-3-.52l2.62 10.726c.122.499-.106 1.028-.589 1.202a5.988 5.988 0 01-2.031.352 5.988 5.988 0 01-2.031-.352c-.483-.174-.711-.703-.59-1.202L18.75 4.971zm-16.5.52c.99-.203 1.99-.377 3-.52m0 0l2.62 10.726c.122.499-.106 1.028-.589 1.202a5.989 5.989 0 01-2.031.352 5.989 5.989 0 01-2.031-.352c-.483-.174-.711-.703-.59-1.202L5.25 4.971z" strokeLinecap="round" strokeLinejoin="round"></path>
                        </svg>
                        <p className="m-auto ms-2">Poids</p>
                    </div>
                    <p className="mb-0 mt-1">{Number(Weight) / 10} kg</p>
                </div>

            </div>
        </>
    )
}