import { ProgressBar } from "react-bootstrap";

interface Props {
    pkmnStats: number;
    statsTxt: string;
}

export default function StatsBar({pkmnStats,statsTxt}:Props) {

    let variant;

    if (pkmnStats < 70) {
      variant = 'danger'; // Couleur rouge pour les statistiques faibles
    } else if (pkmnStats < 100) {
      variant = 'warning'; // Couleur jaune pour les statistiques moyennes
    } else {
      variant = 'success'; // Couleur verte pour les statistiques élevées
    }
    
  

    return (
        <>
            <div className="d-flex flex-row justify-content-between mb-3 align-items-center">

                <div className="col-2 d-flex flex-colum justify-content-between fw-bold text-right opacity-50 fs-6">
                <p className="mb-0">{statsTxt}</p>
                </div>

                <div className="fw-bold text-right fs-6">
                    <p className="mb-0">{pkmnStats}</p>
                </div>

                <div className="d-block col-6 col-sm-8" style={{marginBottom : 'auto', marginTop : 'auto'}}>
                    <ProgressBar now={pkmnStats} max={255}  variant={variant}/>
                </div>

            </div>
        </>
    )
}