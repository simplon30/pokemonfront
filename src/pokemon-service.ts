import axios from "axios";
import { Pokemon } from "./entities";



export async function fetchPokemonById(id : number){
    const response = await axios.get<Pokemon>('http://localhost:8000/api/pokemon/id/'+id);
    return response.data;
}


export async function fetchPokemonByName(nom : string){
    const response = await axios.get<Pokemon>('http://localhost:8000/api/pokemon/'+nom);
    return response.data;
}
