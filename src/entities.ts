export interface Pokedex {
    id: number;
    nom: string;
    couleur: string;
    type: string;
    sprite: string;
    spriteShiny: string;
}

export interface Pokemon {
    pokemon : PokemonData;
    evolution? : Evolution[];
}

export interface PokemonData {
    id?: number;
    nom: string;
    couleur: string;
    type: string;
    sprite: string;
    spriteShiny: string;
    description: string;
    height: string;
    weight: string;
    abilities: string;
    abilitiesDesc: string;
    encounters: string;
    Hp: number;
    Attack: number;
    Defense: number;
    specialAttack: number;
    specialDefense: number;
    speed: number;
    eggGroups: string;
    genderRate: number;
    captureRate: number;
    genera: string;
    shape: string;
    evolutionChainID: number;
}

export interface Evolution {
    id : number;
    idEvolutionChain: number ;
    evolveFromName: string ;
    evolveFromImg: string ;
    evolveToName: string ;
    evolveToImage: string ;
    evolveToLvl?: number ;
    evolveToCondition?: string ;
    evolveToBonheur?: number ;
    evolveToTimeOfDay?: string ;
}
